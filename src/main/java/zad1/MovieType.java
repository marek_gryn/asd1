package zad1;

/**
 * Created by Marek on 16.08.2017.
 */
public enum MovieType {
    ACTION,
    DRAMA,
    COMEDY,
    HORROR;
}
