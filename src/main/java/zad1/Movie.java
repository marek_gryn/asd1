package zad1;

import java.util.Date;

/**
 * Created by Marek on 16.08.2017.
 */
public class Movie {
    private String name;
    private MovieType movieType;
    private Date releaseDate;
    private String author;

    public Movie(String name, MovieType movieType, Date releaseDate, String author) {
        this.name = name;
        this.movieType = movieType;
        this.releaseDate = releaseDate;
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MovieType getMovieType() {
        return movieType;
    }

    public void setMovieType(MovieType movieType) {
        this.movieType = movieType;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "name='" + name + '\'' +
                ", movieType=" + movieType +
                ", releaseDate=" + releaseDate +
                ", author='" + author + '\'' +
                '}';
    }
}
