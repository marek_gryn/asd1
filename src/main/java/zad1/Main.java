package zad1;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

/**
 * Created by RENT on 2017-08-16.
 */
public class Main {
    public static void main(String[] args) {
        MovieDatabase movieDatabase = new MovieDatabase();
        Scanner scanner = new Scanner(System.in);
        String commandLine;
        String command;
        String[] commandLineAfterSplit;

        do {
            commandLine = scanner.nextLine();
            commandLineAfterSplit = commandLine.split(" ");
            command = commandLineAfterSplit[0];
            switch (command) {
                case "add":
                    String name0 = commandLineAfterSplit[1];
                    MovieType movieType0 = MovieType.valueOf(commandLineAfterSplit[2]);
                    String author0 = commandLineAfterSplit[3];
                    movieDatabase.addMovie(new Movie(name0,movieType0, new Date(), author0));
                    break;
                case "search":
                    String name = commandLineAfterSplit[1];
                    movieDatabase.returnMovieFromMap(name);
                    break;
                case "show":
                    movieDatabase.printAllMovies();
                    break;
                case "filter":
                    List<Movie> movies = new ArrayList<>();

                    movieDatabase.printAllMovies();
                    break;
                default:
                    break;

            }
        } while (!command.equals("quit"));

    }
}
