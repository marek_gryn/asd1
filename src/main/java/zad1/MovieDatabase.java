package zad1;

import java.util.*;

/**
 * Created by Marek on 16.08.2017.
 */
public class MovieDatabase {
    private Map<String, Movie> moviesMap = new HashMap<String, Movie>();

    public void addMovie(Movie m){
        moviesMap.put(m.getName(),m);
    }

    public Optional<Movie> returnMovieFromMap (String movie){
        if (moviesMap.containsKey(movie)){
            return Optional.of(moviesMap.get(movie));
        }
        else {
            System.out.println("Szukanego filmu nie ma w bazie danych");
            return Optional.empty();
        }
    }

    public List<Movie> returnMoviesOfGivenType (MovieType movieType){
        List<Movie> moviesList=new ArrayList<>();
        for (Map.Entry<String, Movie> entry:moviesMap.entrySet()){
            if (entry.getValue().getMovieType()==movieType);
        }
        return moviesList;
    }

    public void printAllMovies (){
        for (Map.Entry<String, Movie> entry:moviesMap.entrySet()){
            entry.getValue();
        }
    }
}
